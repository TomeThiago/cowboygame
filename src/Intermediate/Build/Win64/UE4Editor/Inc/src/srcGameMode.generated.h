// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SRC_srcGameMode_generated_h
#error "srcGameMode.generated.h already included, missing '#pragma once' in srcGameMode.h"
#endif
#define SRC_srcGameMode_generated_h

#define src_Source_src_srcGameMode_h_12_SPARSE_DATA
#define src_Source_src_srcGameMode_h_12_RPC_WRAPPERS
#define src_Source_src_srcGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define src_Source_src_srcGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAsrcGameMode(); \
	friend struct Z_Construct_UClass_AsrcGameMode_Statics; \
public: \
	DECLARE_CLASS(AsrcGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/src"), SRC_API) \
	DECLARE_SERIALIZER(AsrcGameMode)


#define src_Source_src_srcGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAsrcGameMode(); \
	friend struct Z_Construct_UClass_AsrcGameMode_Statics; \
public: \
	DECLARE_CLASS(AsrcGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/src"), SRC_API) \
	DECLARE_SERIALIZER(AsrcGameMode)


#define src_Source_src_srcGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SRC_API AsrcGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AsrcGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SRC_API, AsrcGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AsrcGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SRC_API AsrcGameMode(AsrcGameMode&&); \
	SRC_API AsrcGameMode(const AsrcGameMode&); \
public:


#define src_Source_src_srcGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SRC_API AsrcGameMode(AsrcGameMode&&); \
	SRC_API AsrcGameMode(const AsrcGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SRC_API, AsrcGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AsrcGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AsrcGameMode)


#define src_Source_src_srcGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define src_Source_src_srcGameMode_h_9_PROLOG
#define src_Source_src_srcGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Source_src_srcGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Source_src_srcGameMode_h_12_SPARSE_DATA \
	src_Source_src_srcGameMode_h_12_RPC_WRAPPERS \
	src_Source_src_srcGameMode_h_12_INCLASS \
	src_Source_src_srcGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define src_Source_src_srcGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Source_src_srcGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Source_src_srcGameMode_h_12_SPARSE_DATA \
	src_Source_src_srcGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	src_Source_src_srcGameMode_h_12_INCLASS_NO_PURE_DECLS \
	src_Source_src_srcGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SRC_API UClass* StaticClass<class AsrcGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID src_Source_src_srcGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
