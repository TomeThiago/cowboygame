// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "srcGameMode.h"
#include "srcCharacter.h"
#include "UObject/ConstructorHelpers.h"

AsrcGameMode::AsrcGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
