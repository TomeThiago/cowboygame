// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "srcGameMode.generated.h"

UCLASS(minimalapi)
class AsrcGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AsrcGameMode();
};



